import type { PublicKeys } from '@partisiablockchain/snap/src/keyDerivation';
import { useState } from 'react';
import styled from 'styled-components';

import {
  Card,
  ConnectButton,
  InstallFlaskButton,
  InteractButton,
} from '../components';
import { defaultSnapOrigin } from '../config';
import {
  useInvokeSnap,
  useMetaMask,
  useMetaMaskContext,
  useRequestSnap,
} from '../hooks';
import { isLocalSnap, shouldDisplayReconnectButton } from '../utils';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
  margin-top: 7.6rem;
  margin-bottom: 7.6rem;

  ${({ theme }) => theme.mediaQueries.small} {
    padding-left: 2.4rem;
    padding-right: 2.4rem;
    margin-top: 2rem;
    margin-bottom: 2rem;
    width: auto;
  }
`;

const Heading = styled.h1`
  margin-top: 0;
  margin-bottom: 2.4rem;
  text-align: center;
`;

const Span = styled.span`
  color: ${(props) => props.theme.colors.primary?.default};
`;

const Subtitle = styled.p`
  font-size: ${({ theme }) => theme.fontSizes.large};
  font-weight: 500;
  margin-top: 0;
  margin-bottom: 0;

  ${({ theme }) => theme.mediaQueries.small} {
    font-size: ${({ theme }) => theme.fontSizes.text};
  }
`;

const CardContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  max-width: 134rem;
  width: 100%;
  height: 100%;
  margin-top: 1.5rem;
`;

const Notice = styled.div`
  background-color: ${({ theme }) => theme.colors.background?.alternative};
  border: 1px solid ${({ theme }) => theme.colors.border?.default};
  color: ${({ theme }) => theme.colors.text?.alternative};
  border-radius: ${({ theme }) => theme.radii.default};
  padding: 2.4rem;
  margin-top: 2.4rem;
  width: 100%;

  & > * {
    margin: 0;
  }

  ${({ theme }) => theme.mediaQueries.small} {
    margin-top: 1.2rem;
    padding: 1.6rem;
  }
`;

const ErrorMessage = styled.div`
  background-color: ${({ theme }) => theme.colors.error?.muted};
  border: 1px solid ${({ theme }) => theme.colors.error?.default};
  color: ${({ theme }) => theme.colors.error?.alternative};
  border-radius: ${({ theme }) => theme.radii.default};
  padding: 2.4rem;
  margin-bottom: 2.4rem;
  margin-top: 2.4rem;
  width: 100%;

  ${({ theme }) => theme.mediaQueries.small} {
    padding: 1.6rem;
    margin-bottom: 1.2rem;
    margin-top: 1.2rem;
    max-width: 100%;
  }
`;

const Index = () => {
  const [signature, setSignature] = useState<unknown>();
  const [address, setAddress] = useState<unknown>();
  const [accounts, setAccounts] = useState<PublicKeys | null>(null);
  const [payload, setPayload] = useState<string>(
    '00000000000001fd0000018f5843cf9300000000000186a002c14c29b2697f3c983ada0ee7fac83f8a937e2ecd0000001a89fae4ff01008d4296154ed5cdb25130d7e188ca6b7e74032a17',
  );
  const [chooseAccountIndex, setChooseAccountIndex] = useState<string>('0');

  const { error } = useMetaMaskContext();
  const { isFlask, snapsDetected, installedSnap } = useMetaMask();
  const requestSnap = useRequestSnap();
  const invokeSnap = useInvokeSnap();

  const isMetaMaskReady = isLocalSnap(defaultSnapOrigin)
    ? isFlask
    : snapsDetected;

  const signTransaction = async () => {
    const mySignature = await invokeSnap({
      method: 'sign_transaction',
      params: { payload, chainId: 'Partisia Blockchain' },
    });
    setSignature(mySignature);
  };

  const getCurrentAddress = async () => {
    const myAddress = await invokeSnap({
      method: 'get_address',
    });
    setAddress(myAddress);
  };

  const getAccounts = async () => {
    const publicKeys = await invokeSnap({
      method: 'get_accounts',
    });
    setAccounts(publicKeys as PublicKeys | null);
  };

  const chooseAccount = async () => {
    const publicKeys = await invokeSnap({
      method: 'choose_account',
      params: { addressIndex: Number(chooseAccountIndex) },
    });
    setAccounts(publicKeys as PublicKeys | null);
  };

  const createAccount = async () => {
    const publicKeys = await invokeSnap({
      method: 'create_account',
    });
    setAccounts(publicKeys as PublicKeys | null);
  };

  const removeAccount = async () => {
    const publicKeys = await invokeSnap({
      method: 'remove_account',
    });
    setAccounts(publicKeys as PublicKeys | null);
  };

  return (
    <Container>
      <Heading>
        Welcome to <Span>partisiablockchain snap</Span>
      </Heading>
      <Subtitle>
        Get started by editing <code>src/index.tsx</code>
      </Subtitle>
      <CardContainer>
        <>
          {error && (
            <ErrorMessage>
              <b>An error happened:</b> {error.message}
            </ErrorMessage>
          )}
          {signature && (
            <Notice>
              <>
                <b>Latest signature:</b> {signature}
              </>
            </Notice>
          )}
          {accounts && (
            <>
              <Card
                content={{
                  title: 'Selected account',
                  description: (
                    <>
                      #{accounts.selected.addressIndex}
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      {accounts.selected.key}
                      <br />
                    </>
                  ),
                }}
              />
              <Card
                content={{
                  title: 'Other accounts',
                  description: (
                    <>
                      {accounts.other.map((account) => (
                        <>
                          #{account.addressIndex}
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          {account.key}
                          <br />
                        </>
                      ))}
                    </>
                  ),
                }}
              />
            </>
          )}
          {!isMetaMaskReady && (
            <Card
              content={{
                title: 'Install',
                description:
                  'Snaps is pre-release software only available in MetaMask Flask, a canary distribution for developers with access to upcoming features.',
                button: <InstallFlaskButton />,
              }}
              fullWidth
            />
          )}
          {!installedSnap && (
            <Card
              content={{
                title: 'Connect',
                description:
                  'Get started by connecting to and installing the partisiablockchain snap.',
                button: (
                  <ConnectButton
                    onClick={requestSnap}
                    disabled={!isMetaMaskReady}
                  />
                ),
              }}
              disabled={!isMetaMaskReady}
            />
          )}
          <Card
            content={{
              title: 'Get account',
              description:
                address === undefined
                  ? 'Get your current PBC account address'
                  : `Address: ${String(address)}`,
              button: (
                <InteractButton
                  onClick={getCurrentAddress}
                  disabled={!installedSnap}
                >
                  {'Get address'}
                </InteractButton>
              ),
            }}
            disabled={!installedSnap}
            fullWidth={
              isMetaMaskReady &&
              Boolean(installedSnap) &&
              !shouldDisplayReconnectButton(installedSnap)
            }
          />
          <Card
            content={{
              title: 'Get all accounts',
              description: <>'Get all your accounts'</>,
              button: (
                <InteractButton onClick={getAccounts} disabled={!installedSnap}>
                  {'Get all accounts'}
                </InteractButton>
              ),
            }}
            disabled={!installedSnap}
            fullWidth={
              isMetaMaskReady &&
              Boolean(installedSnap) &&
              !shouldDisplayReconnectButton(installedSnap)
            }
          />
          <Card
            content={{
              title: 'Create account',
              description: 'Adds an additional PBC account to your snap',
              button: (
                <InteractButton
                  onClick={createAccount}
                  disabled={!installedSnap}
                >
                  {'Add account'}
                </InteractButton>
              ),
            }}
            disabled={!installedSnap}
            fullWidth={
              isMetaMaskReady &&
              Boolean(installedSnap) &&
              !shouldDisplayReconnectButton(installedSnap)
            }
          />
          <Card
            content={{
              title: 'Remove account',
              description:
                'Removes the account with the highest index from your snap',
              button: (
                <InteractButton
                  onClick={removeAccount}
                  disabled={!installedSnap}
                >
                  {'Remove account'}
                </InteractButton>
              ),
            }}
            disabled={!installedSnap}
            fullWidth={
              isMetaMaskReady &&
              Boolean(installedSnap) &&
              !shouldDisplayReconnectButton(installedSnap)
            }
          />
          <Card
            content={{
              title: 'Choose account',
              description: (
                <input
                  style={{ width: '100%' }}
                  type="number"
                  value={chooseAccountIndex}
                  onChange={(changeEvent) =>
                    setChooseAccountIndex(changeEvent.target.value)
                  }
                />
              ),
              button: (
                <InteractButton
                  onClick={chooseAccount}
                  disabled={!installedSnap}
                >
                  {'Choose account'}
                </InteractButton>
              ),
            }}
            disabled={!installedSnap}
            fullWidth={
              isMetaMaskReady &&
              Boolean(installedSnap) &&
              !shouldDisplayReconnectButton(installedSnap)
            }
          />
          <Card
            content={{
              title:
                'Sign a transaction payload in hex towards PartisiaBlockchain from your account',
              description: (
                <input
                  style={{ width: '100%' }}
                  type="text"
                  value={payload}
                  onChange={(changeEvent) =>
                    setPayload(changeEvent.target.value)
                  }
                />
              ),
              button: (
                <InteractButton
                  onClick={signTransaction}
                  disabled={!installedSnap}
                >
                  {'Sign transaction'}
                </InteractButton>
              ),
            }}
            disabled={!installedSnap}
            fullWidth={
              isMetaMaskReady &&
              Boolean(installedSnap) &&
              !shouldDisplayReconnectButton(installedSnap)
            }
          />
          <Notice>
            <p>
              Please note that the <b> snap.manifest.json</b> and{' '}
              <b>package.json</b> must be located in the server root directory
              and the bundle must be hosted at the location specified by the
              location field.
            </p>
          </Notice>
        </>
      </CardContainer>
    </Container>
  );
};

export default Index;
