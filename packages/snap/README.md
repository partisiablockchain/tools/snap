# Partisia Blockchain snap

Snap used with Partisia Blockchain.

## How do I use the snap

Are you a dapp developer and end-user you should visit
the [Partisia Blockchain documentation](https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/metamask-snap-integration.html).
