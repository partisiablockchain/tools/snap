import type { DialogParams } from '@metamask/snaps-sdk';
import { Box, Divider, Heading, Text } from '@metamask/snaps-sdk/jsx';

/**
 * Create a confirmation dialog for the create account rpc request.
 * @param switchToNewAccount - Whether to switch to the newly created account.
 * @returns Dialog params for generating the dialog.
 */
export function createAccountConfirmationDialog(
  switchToNewAccount: boolean,
): DialogParams {
  return {
    type: 'confirmation',
    content: (
      <Box>
        <Heading>{'Add a new account'}</Heading>
        <Text>{'Are you sure you want to add a new account?'}</Text>
        <Divider />
        <Text>
          {switchToNewAccount
            ? 'The new account will automatically be selected when you add it'
            : 'The new account will not be selected when you add it'}
        </Text>
      </Box>
    ),
  };
}
