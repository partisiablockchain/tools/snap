import type { DialogParams } from '@metamask/snaps-sdk';
import type { JSXElement } from '@metamask/snaps-sdk/jsx';
import { Box, Divider, Heading, Row, Text } from '@metamask/snaps-sdk/jsx';

const CurrentAccountWarning = (props: {
  isCurrentAccount: boolean;
}): JSXElement => {
  if (props.isCurrentAccount) {
    return (
      <Box>
        <Divider />
        <Row
          variant={'critical'}
          label={
            'This is your current active account, if removed the active account will be switched to the default account'
          }
        >
          <Text>{''}</Text>
        </Row>
      </Box>
    );
  }
  return <Text>{''}</Text>;
};

/**
 * Create a confirmation dialog for the removing an account.
 * @param accountAddress - The address of the account that is being removed.
 * @param isCurrentAccount - Whether it is the active account that is being removed.
 * @returns Dialog params for generating the dialog.
 */
export function removeAccountConfirmationDialog(
  accountAddress: string,
  isCurrentAccount: boolean,
): DialogParams {
  return {
    type: 'confirmation',
    content: (
      <Box>
        <Heading>{'Remove account'}</Heading>
        <Text>{'Are you sure you want to remove the following account'}</Text>
        <Row variant={'warning'} label={accountAddress}>
          <Text>{''}</Text>
        </Row>
        <CurrentAccountWarning isCurrentAccount={isCurrentAccount} />
        <Divider />
        <Text>
          {
            'You can always get this account back by adding a new account again, this will derive the same account'
          }
        </Text>
      </Box>
    ),
  };
}

/**
 * Create an alert telling the user that he cannot remove an account when they only have one
 * account.
 * @returns Dialog params for generating the dialog.
 */
export function removeOnlyAccountAlert(): DialogParams {
  return {
    type: 'alert',
    content: (
      <Box>
        <Heading>{'Remove account'}</Heading>
        <Text>
          {
            'You cannot remove any more accounts, as you currently only have one account'
          }
        </Text>
      </Box>
    ),
  };
}
