import type { DialogParams } from '@metamask/snaps-sdk';
import { heading, panel, text } from '@metamask/snaps-sdk';

/**
 * Create a confirmation dialog for the sign transaction rpc request.
 *
 * @param chainId - The chain it is being signed for.
 * @param nonce - The nonce of the request.
 * @param gasCost - The cast of the request.
 * @param target - The contract the transaction is being made on.
 * @param accountAddress - The address of the account the user is signing with.
 * @returns Dialog params for generating the dialog.
 */
export function signTransactionConfirmationDialog(
  chainId: string,
  nonce: string,
  gasCost: string,
  target: string,
  accountAddress: string,
): DialogParams {
  return {
    type: 'confirmation',
    content: panel([
      heading(`Confirm transaction signature on ${chainId}`),
      text(`You are currently using account: ${accountAddress}`),
      text(
        `Sign transaction with nonce ${nonce} and cost ${gasCost} towards contract ${target}`,
      ),
    ]),
  };
}
