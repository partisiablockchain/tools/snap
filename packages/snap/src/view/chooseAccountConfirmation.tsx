import type { DialogParams } from '@metamask/snaps-sdk';
import { Box, Heading, Text } from '@metamask/snaps-sdk/jsx';

/**
 * Create a confirmation dialog for choosing an account.
 *
 * @param chosenAccount - The contract the transaction is being made on.
 * @returns Dialog params for generating the dialog.
 */
export function chooseAccountConfirmationDialog(
  chosenAccount: string,
): DialogParams {
  return {
    type: 'confirmation',
    content: (
      <Box>
        <Heading>{'Choose account'}</Heading>
        <Text>
          {'Are you sure you want to switch to the following PBC account?'}
        </Text>
        <Text>{chosenAccount}</Text>
      </Box>
    ),
  };
}
