import { UserRejectedRequestError } from '@metamask/snaps-sdk';

import type { PublicKeys } from './keyDerivation';
import {
  getPrivateKey,
  privateKeyToAccountAddress,
  getPublicKeys,
} from './keyDerivation';
import {
  getNumberOfAccounts,
  getSelectedAccountIndex,
  removeAccount,
} from './multipleAccounts';
import {
  removeOnlyAccountAlert,
  removeAccountConfirmationDialog,
} from './view/removeAccountConfirmation';

/**
 * Handle a create account rpc request.
 * @returns The new public keys.
 * @throws UserRejectedRequestError if the user rejects the account creation.
 */
export async function handleRemoveAccount(): Promise<PublicKeys> {
  const numberOfAccounts = await getNumberOfAccounts();

  if (numberOfAccounts < 2) {
    await snap.request({
      method: 'snap_dialog',
      params: removeOnlyAccountAlert(),
    });
  } else {
    const currentAccount = await getSelectedAccountIndex();
    const indexToBeRemoved = numberOfAccounts - 1;
    const isCurrentAccount = currentAccount === indexToBeRemoved;
    const accountToBeRemoved = privateKeyToAccountAddress(
      await getPrivateKey(indexToBeRemoved),
    );
    const userConfirmed = await snap.request({
      method: 'snap_dialog',
      params: removeAccountConfirmationDialog(
        accountToBeRemoved,
        isCurrentAccount,
      ),
    });
    if (userConfirmed) {
      await removeAccount();
    } else {
      throw new UserRejectedRequestError();
    }
  }
  return getPublicKeys();
}
