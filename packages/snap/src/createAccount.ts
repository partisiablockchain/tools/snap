import { UserRejectedRequestError } from '@metamask/snaps-sdk';

import type { PublicKeys } from './keyDerivation';
import { getPublicKeys } from './keyDerivation';
import { createAccount } from './multipleAccounts';
import { createAccountConfirmationDialog } from './view/createAccountConfirmation';

/**
 * Handle a create account rpc request.
 * @param requestParams - The given rpc arguments.
 * @returns The new public keys.
 * @throws UserRejectedRequestError if the user rejects the account creation.
 */
export async function handleCreateAccount(
  requestParams: unknown,
): Promise<PublicKeys> {
  const switchToNewAccount = getSwitchAccountParam(requestParams);

  const userConfirmed = await snap.request({
    method: 'snap_dialog',
    params: createAccountConfirmationDialog(switchToNewAccount),
  });

  if (userConfirmed) {
    await createAccount(switchToNewAccount);
    return getPublicKeys();
  }
  throw new UserRejectedRequestError();
}

/**
 * Determine whether to switch to the new account or not from the request parameters.
 * @param params - The given rpc params.
 * @returns True if params has valid format and switchToNewAccount is set.
 */
function getSwitchAccountParam(params: unknown) {
  if (
    params === null ||
    params === undefined ||
    typeof params !== 'object' ||
    !('switchToNewAccount' in params)
  ) {
    return false;
  }

  const paramObject = params as { switchToNewAccount: unknown };
  if (typeof paramObject.switchToNewAccount === 'boolean') {
    return Boolean(paramObject.switchToNewAccount);
  }
  return false;
}
