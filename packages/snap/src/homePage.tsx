import type { OnHomePageHandler } from '@metamask/snaps-sdk';
import {
  Box,
  Copyable,
  Heading,
  Link,
  Section,
  Text,
} from '@metamask/snaps-sdk/jsx';

import type { PublicKey } from './keyDerivation';
import { getAllPublicKeys } from './keyDerivation';
import { getSelectedAccountIndex } from './multipleAccounts';

const AccountList = (props: {
  publicKeys: PublicKey[];
  selectedAccount: number;
}) => {
  const { publicKeys, selectedAccount } = props;

  const onlyOneKey = publicKeys.length === 1;

  return (
    <Box>
      <Heading>{onlyOneKey ? 'Your account' : 'Accounts'}</Heading>
      {publicKeys.map((publicKey, i) => {
        if (!onlyOneKey && i === selectedAccount) {
          return (
            <Section key={`${i}-${publicKey.key}`}>
              <Text>{'Current account'}</Text>
              <Copyable value={publicKey.key} />
            </Section>
          );
        }
        return <Copyable value={publicKey.key} key={`${i}-${publicKey.key}`} />;
      })}
    </Box>
  );
};

export const onHomePage: OnHomePageHandler = async () => {
  const publicKeys = await getAllPublicKeys();
  const selectedAccount = await getSelectedAccountIndex();

  return {
    content: (
      <Box>
        <Box direction={'horizontal'} alignment={'space-between'}>
          <Heading>{'See your assets'}</Heading>
          <Link href={'https://browser.partisiablockchain.com/assets'}>
            {'Assets'}
          </Link>
        </Box>
        <Box direction={'horizontal'} alignment={'space-between'}>
          <Heading>{'Bridge your coins'}</Heading>
          <Link href={'https://browser.partisiablockchain.com/bridge'}>
            {'Bridge'}
          </Link>
        </Box>
        <AccountList
          publicKeys={publicKeys}
          selectedAccount={selectedAccount}
        />
      </Box>
    ),
  };
};
