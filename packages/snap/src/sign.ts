import { Buffer } from 'buffer';
import type { ec as Elliptic } from 'elliptic';
import { sha256 } from 'hash.js';

import { privateKeyToKeypair } from './keyDerivation';

/**
 * Convert a string chain id to a buffer.
 * @param chainId - The chainId to convert.
 * @returns A buffer containing the chain id.
 */
function chainIdAsBuffer(chainId: string): Buffer {
  const strBuffer = Buffer.from(chainId, 'utf8');
  const length = Buffer.alloc(4);
  length.writeInt32BE(strBuffer.length);
  return Buffer.concat([length, strBuffer]);
}

/**
 * Sign a transaction.
 * @param txPayload - The payload of the transaction.
 * @param chainId - The chain id of the chain to receive the transaction.
 * @param privateKey - The private key to use for signing.
 * @returns The signature as a hex string.
 */
export function signTransaction(
  txPayload: Buffer,
  chainId: string,
  privateKey: string,
): string {
  const hash = sha256();
  hash.update(txPayload);
  hash.update(chainIdAsBuffer(chainId));
  return signBuffer(Buffer.from(hash.digest()), privateKey);
}

/**
 * Sign a buffer using a private key.
 * @param data - The data to sign.
 * @param privateKey - The private key to use when signing.
 * @returns Signature as hex string.
 */
function signBuffer(data: Buffer, privateKey: string): string {
  const keyPair = privateKeyToKeypair(privateKey);
  const signature = keyPair.sign(data);
  return signatureToBuffer(signature).toString('hex');
}

/**
 * Convert a signature to a buffer in format expected by Partisia Blockchain.
 * @param signature - The signature to convert.
 * @returns A buffer containing the signature.
 */
export function signatureToBuffer(signature: Elliptic.Signature): Buffer {
  if (signature.recoveryParam === null) {
    throw new Error('Recovery parameter is null');
  }
  return Buffer.concat([
    Buffer.from([signature.recoveryParam]),
    signature.r.toArrayLike(Buffer, 'be', 32),
    signature.s.toArrayLike(Buffer, 'be', 32),
  ]);
}
