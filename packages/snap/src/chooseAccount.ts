import {
  InvalidParamsError,
  UserRejectedRequestError,
} from '@metamask/snaps-sdk';

import { getPrivateKey, privateKeyToAccountAddress } from './keyDerivation';
import { chooseAccount } from './multipleAccounts';
import { chooseAccountConfirmationDialog } from './view/chooseAccountConfirmation';

/**
 * Handle a choose account rpc request.
 * @param requestParams - The given rpc arguments.
 * @returns A void promise.
 * @throws InvalidParamsError if the parameters are malformed.
 * @throws UserRejectedRequestError if the user rejects the signing.
 */
export async function handleChooseAccount(requestParams: unknown) {
  if (!validChooseAccountParams(requestParams)) {
    throw new InvalidParamsError('Malformed choose account request');
  }

  const privateKey = await getPrivateKey(requestParams.addressIndex);
  const address = privateKeyToAccountAddress(privateKey);

  const userConfirmed = await snap.request({
    method: 'snap_dialog',
    params: chooseAccountConfirmationDialog(address),
  });

  if (userConfirmed) {
    return await chooseAccount(requestParams.addressIndex);
  }
  throw new UserRejectedRequestError();
}

type ChooseAccountParams = {
  addressIndex: number;
};

/**
 * Verify the format of parameters passed to choose account method.
 * @param params - The params to verify.
 * @returns True if params has valid format.
 */
function validChooseAccountParams(
  params: unknown,
): params is ChooseAccountParams {
  if (
    params === null ||
    params === undefined ||
    typeof params !== 'object' ||
    !('addressIndex' in params)
  ) {
    return false;
  }

  const paramsObject = params as { addressIndex: unknown };
  return typeof paramsObject.addressIndex === 'number';
}
