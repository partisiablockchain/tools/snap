type Format = {
  address: (address: string) => string;
};

/**
 * Hook for getting all formatting methods.
 * @returns An object with all the formatting methods.
 */
export function useFormat(): Format {
  return {
    address: (address: string): string => {
      const first = address.substring(0, 5);
      const last = address.substring(address.length - 3);
      return `${first}...${last}`;
    },
  };
}
