import { ManageStateOperation } from '@metamask/snaps-sdk';

export type State = {
  selectedAccountIndex: number;
  numberOfAccounts: number;
};

/**
 * The default state of the snap. This is returned by the {@link getState}
 * function if the state has not been set yet.
 */
const DEFAULT_STATE: State = {
  selectedAccountIndex: 0,
  numberOfAccounts: 1,
};

/**
 * Get the current state of the snap. If the snap does not have state, the
 * {@link DEFAULT_STATE} is returned instead.
 *
 * This uses the `snap_manageState` JSON-RPC method to get the state.
 * @see https://docs.metamask.io/snaps/reference/rpc-api/#snap_managestate
 * @param encrypted - An optional flag to indicate whether to use encrypted storage or not.
 * The encrypted storage is separate from the non-encrypted storage.
 * @returns The current state of the snap.
 */
export async function getState(encrypted = true): Promise<State> {
  const state = await snap.request({
    method: 'snap_manageState',

    params: {
      operation: ManageStateOperation.GetState,
      encrypted,
    },
  });

  // If the snap does not have state, `state` will be `null`. Instead, we return
  // the default state.
  return (state as State | null) ?? DEFAULT_STATE;
}

/**
 * Set the state of the snap. This will overwrite the current state.
 *
 * This uses the `snap_manageState` JSON-RPC method to set the state. The state
 * is encrypted with the user's secret recovery phrase and stored in the user's
 * browser.
 * @see https://docs.metamask.io/snaps/reference/rpc-api/#snap_managestate
 * @param newState - The new state of the snap.
 * @param encrypted - An optional flag to indicate whether to use encrypted
 * storage or not. Unencrypted storage does not require the user to unlock
 * MetaMask in order to access it, but it should not be used for sensitive data.
 * Defaults to true.
 */
export async function setState(newState: State, encrypted = true) {
  await snap.request({
    method: 'snap_manageState',

    params: {
      operation: ManageStateOperation.UpdateState,
      newState,
      encrypted,
    },
  });
}

/**
 * Update the state of the snap. This will retrieve the current state, use the given update
 * method on it, and set the new state.
 *
 * This uses the `snap_manageState` JSON-RPC method to get and set the state. The state
 * is encrypted with the user's secret recovery phrase and stored in the user's
 * browser.
 * @see https://docs.metamask.io/snaps/reference/rpc-api/#snap_managestate
 * @param update - Method to update the state or the new fields to add to the state.
 * @param encrypted - An optional flag to indicate whether to use encrypted
 * storage or not. Unencrypted storage does not require the user to unlock
 * MetaMask in order to access it, but it should not be used for sensitive data.
 * Defaults to true.
 */
export async function updateState(
  update: ((prevState: State) => Partial<State>) | Partial<State>,
  encrypted = true,
) {
  const prevState = await getState(encrypted);

  let newState: State;
  if (update instanceof Function) {
    newState = {
      ...prevState,
      ...update(prevState),
    };
  } else {
    newState = {
      ...prevState,
      ...update,
    };
  }
  await setState(newState, encrypted);
}

/**
 * Clear the state of the snap. This will set the state to the
 * {@link DEFAULT_STATE}.
 *
 * This uses the `snap_manageState` JSON-RPC method to clear the state.
 * @see https://docs.metamask.io/snaps/reference/rpc-api/#snap_managestate
 * @param encrypted - An optional flag to indicate whether to use encrypted storage or not.
 */
export async function clearState(encrypted = true) {
  await snap.request({
    method: 'snap_manageState',

    params: {
      operation: ManageStateOperation.ClearState,
      encrypted,
    },
  });
}
