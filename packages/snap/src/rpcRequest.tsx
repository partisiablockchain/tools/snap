import type { OnRpcRequestHandler } from '@metamask/snaps-sdk';
import { MethodNotFoundError } from '@metamask/snaps-sdk';

import { handleChooseAccount } from './chooseAccount';
import { handleCreateAccount } from './createAccount';
import {
  getPrivateKey,
  getPublicKeys,
  privateKeyToAccountAddress,
} from './keyDerivation';
import { getSelectedAccountIndex } from './multipleAccounts';
import { handleRemoveAccount } from './removeAccount';
import { handleSignTransaction } from './signTransaction';

/**
 * Handle incoming JSON-RPC requests, sent through `wallet_invokeSnap`.
 * @param args - The request handler args as object.
 * @param args.request - A validated JSON-RPC request object.
 * @returns The result of request.
 * @throws If the request method is not valid for this snap.
 */
export const onRpcRequest: OnRpcRequestHandler = async ({ request }) => {
  switch (request.method) {
    case 'get_address': {
      const accountIndex = await getSelectedAccountIndex();
      const privateKey = await getPrivateKey(accountIndex);
      return privateKeyToAccountAddress(privateKey);
    }

    case 'get_accounts': {
      return await getPublicKeys();
    }

    case 'choose_account': {
      const { params } = request;
      await handleChooseAccount(params);
      return getPublicKeys();
    }

    case 'create_account': {
      const { params } = request;
      return handleCreateAccount(params);
    }

    case 'remove_account': {
      return handleRemoveAccount();
    }

    case 'sign_transaction': {
      const { params } = request;
      return await handleSignTransaction(params);
    }
    default:
      throw new MethodNotFoundError({ method: request.method });
  }
};
