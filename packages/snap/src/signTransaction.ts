import {
  InvalidParamsError,
  UserRejectedRequestError,
} from '@metamask/snaps-sdk';
import BN from 'bn.js';
import { Buffer } from 'buffer';

import { getPrivateKey, privateKeyToAccountAddress } from './keyDerivation';
import { getSelectedAccountIndex } from './multipleAccounts';
import { signTransaction } from './sign';
import { signTransactionConfirmationDialog } from './view/signTransaction';

/**
 * Handle a sign transaction rpc request.
 * @param requestParams - The given rpc arguments.
 * @returns The signed transaction's signature.
 * @throws InvalidParamsError if the parameters are malformed.
 * @throws UserRejectedRequestError if the user rejects the signing.
 */
export async function handleSignTransaction(
  requestParams: unknown,
): Promise<string> {
  if (validSignParams(requestParams)) {
    const { chainId } = requestParams;
    const txPayload: Buffer = Buffer.from(requestParams.payload, 'hex');
    if (txPayload.length < 45) {
      throw new InvalidParamsError('Incomplete transaction');
    }

    const nonce = new BN(txPayload.subarray(0, 8), 'be').toString(10);
    const gasCost = new BN(txPayload.subarray(16, 24), 'be').toString(10);
    const target = txPayload.toString('hex', 24, 45);

    const accountIndex = await getSelectedAccountIndex();
    const privateKey = await getPrivateKey(accountIndex);

    const userConfirmed = await snap.request({
      method: 'snap_dialog',
      params: signTransactionConfirmationDialog(
        chainId,
        nonce,
        gasCost,
        target,
        privateKeyToAccountAddress(privateKey),
      ),
    });
    if (userConfirmed === true) {
      return signTransaction(txPayload, chainId, privateKey);
    }
    throw new UserRejectedRequestError();
  } else {
    throw new InvalidParamsError('Malformed sign request');
  }
}

type SignParams = {
  payload: string;
  chainId: string;
};

/**
 * Verify the format of parameters passed to sign method.
 * @param params - The params to verify.
 * @returns True if params has valid format.
 */
function validSignParams(params: unknown): params is SignParams {
  return (
    params !== null &&
    params !== undefined &&
    typeof params === 'object' &&
    'payload' in params &&
    'chainId' in params
  );
}
