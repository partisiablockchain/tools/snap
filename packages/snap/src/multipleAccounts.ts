import { rpcErrors } from '@metamask/rpc-errors';

import { getState, updateState } from './metamaskState';

/**
 * Get the current number of accounts from the Snap state.
 * @returns The current count.
 */
export async function getNumberOfAccounts(): Promise<number> {
  const state = await getState(false);
  return state.numberOfAccounts;
}

/**
 * Get the selected account index from storage.
 * @returns The selected account index.
 */
export async function getSelectedAccountIndex(): Promise<number> {
  const state = await getState(false);
  return state.selectedAccountIndex;
}

/**
 * Update the state with the chosen account.
 * @param accountIndex - The index of the chosen account.
 */
export async function chooseAccount(accountIndex: number) {
  await updateState(
    {
      selectedAccountIndex: accountIndex,
    },
    false,
  );
}

/**
 * Create a new account by updating the state.
 * @param switchToNewAccount - Whether to switch to the newly created account.
 */
export async function createAccount(switchToNewAccount: boolean) {
  await updateState((prev) => {
    return {
      numberOfAccounts: prev.numberOfAccounts + 1,
      selectedAccountIndex: switchToNewAccount
        ? prev.numberOfAccounts
        : prev.selectedAccountIndex,
    };
  }, false);
}

/**
 * Remove the account with the highest index. If the account is currently selected this selects
 * account 0 instead.
 * @throws if the user only have one account.
 */
export async function removeAccount() {
  await updateState((prev) => {
    const { numberOfAccounts } = prev;

    if (numberOfAccounts < 2) {
      throw rpcErrors.internal({
        message: 'Tried to remove an account, but only one account exist',
      });
    }

    const updateSelectedAccount =
      prev.selectedAccountIndex === numberOfAccounts - 1;

    const newSelectedIndex = updateSelectedAccount
      ? 0
      : prev.selectedAccountIndex;

    return {
      selectedAccountIndex: newSelectedIndex,
      numberOfAccounts: numberOfAccounts - 1,
    };
  }, false);
}
