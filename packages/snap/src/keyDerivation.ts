import { getBIP44AddressKeyDeriver } from '@metamask/key-tree';
import { rpcErrors } from '@metamask/rpc-errors';
import { InternalError } from '@metamask/snaps-sdk';
import { ec as Elliptic } from 'elliptic';
import { sha256 } from 'hash.js';

import {
  getNumberOfAccounts,
  getSelectedAccountIndex,
} from './multipleAccounts';

const ec = new Elliptic('secp256k1');

/**
 * Get the BIP44 key deriver.
 * @returns The BIP44 key deriver.
 */
async function getKeyDeriver() {
  const keyEntropy = await snap.request({
    method: 'snap_getBip44Entropy',
    params: {
      coinType: 3757,
    },
  });
  return await getBIP44AddressKeyDeriver(keyEntropy);
}

/**
 * Get the private key from wallet.
 * @param addressIndex - The BIP44 address index.
 * @returns The private key.
 */
export async function getPrivateKey(addressIndex: number): Promise<string> {
  const keyDeriver = await getKeyDeriver();
  const derivedBip44Node = await keyDeriver(addressIndex);
  const { privateKey } = derivedBip44Node;
  return convertPrivateKey(privateKey);
}

/**
 * Converts a BIP44 private key to a PBC private key.
 * @param privateKey - The BIP44 private key.
 * @returns The PBC private key.
 */
function convertPrivateKey(privateKey: string | undefined): string {
  if (privateKey === undefined) {
    throw new InternalError('Unable to derive BIP44 key');
  }
  return privateKey.replace(/^0x/u, '');
}

export type PublicKeys = {
  selected: PublicKey;
  other: PublicKey[];
};

export type PublicKey = {
  key: string;
  addressIndex: number;
};

/**
 * Get the users public keys for all created accounts.
 * @returns The users public keys as a list.
 */
export async function getAllPublicKeys(): Promise<PublicKey[]> {
  const numberOfAccounts = await getNumberOfAccounts();
  const keyDeriver = await getKeyDeriver();
  const allPublicKeys: PublicKey[] = [];
  for (let i = 0; i < numberOfAccounts; i++) {
    const derivedBip44Node = await keyDeriver(i);
    const privateKey = convertPrivateKey(derivedBip44Node.privateKey);
    const publicKey = privateKeyToAccountAddress(privateKey);
    const key = {
      addressIndex: i,
      key: publicKey,
    };
    allPublicKeys.push(key);
  }
  return allPublicKeys;
}

/**
 * Get the users public keys for all created accounts.
 * @returns The users public keys, both for the selected account and all other created accounts.
 */
export async function getPublicKeys(): Promise<PublicKeys> {
  const selectedAccount = await getSelectedAccountIndex();
  const allPublicKeys = await getAllPublicKeys();
  const selected = allPublicKeys[selectedAccount];
  if (selected === undefined) {
    throw rpcErrors.internal({
      message: 'Failed to find the selected account in your addresses',
    });
  }
  allPublicKeys.splice(selectedAccount, 1);
  return {
    selected,
    other: allPublicKeys,
  };
}

/**
 * Convert a private key to a blockchain address.
 * @param privateKey - The private key to get address of.
 * @returns The derived address.
 */
export function privateKeyToAccountAddress(privateKey: string): string {
  return keyPairToAccountAddress(privateKeyToKeypair(privateKey));
}

/**
 * Convert a private key to a key pair.
 * @param privateKey - The private key to wrap.
 * @returns The key pair.
 */
export function privateKeyToKeypair(privateKey: string): Elliptic.KeyPair {
  return ec.keyFromPrivate(privateKey, 'hex');
}

/**
 * Convert a key pair to a blockchain address.
 * @param keyPair - The keypair to get address of.
 * @returns The derived address.
 */
function keyPairToAccountAddress(keyPair: Elliptic.KeyPair): string {
  const publicKey = keyPair.getPublic(false, 'array');
  const hash = sha256();
  hash.update(publicKey);
  return `00${hash.digest('hex').substring(24)}`;
}
