import { describe, expect } from '@jest/globals';
import { getBIP44AddressKeyDeriver } from '@metamask/key-tree';

import {
  getPublicKeys,
  privateKeyToAccountAddress,
} from '../src/keyDerivation';
import { snapRequest } from './setupTests';

jest.mock('@metamask/key-tree');
const bip44KeyDeriverMock = getBIP44AddressKeyDeriver as jest.Mock;

describe('privateKeyToAccountAddress', () => {
  it('get address from short key', () => {
    const privateKey = '01';
    expect(privateKeyToAccountAddress(privateKey)).toBe(
      '0035e97a5e078a5a0f28ec96d547bfee9ace803ac0',
    );
  });

  it('get address from long key', () => {
    const privateKey =
      '42834300692d84df068784edb6e26a201022f014631ce20ff189dc969f581953';
    expect(privateKeyToAccountAddress(privateKey)).toBe(
      '0056dabb8c650c2119335489e5eaf4a48023574955',
    );
  });
});

describe('getPublicKeys', () => {
  beforeEach(() => {
    bip44KeyDeriverMock.mockResolvedValue(() => ({
      privateKey: '0x11223344',
    }));
  });

  it('derives one key on empty state', async () => {
    const publicKeys = await getPublicKeys();
    expect(publicKeys.selected.addressIndex).toBe(0);
    expect(publicKeys.selected.key).toBe(
      '00d7e42ba9e815a3040584be2b4509e657e6601a3a',
    );
  });

  it('derives the correct number of accounts', async () => {
    snapRequest.mockResolvedValue({
      numberOfAccounts: 3,
      selectedAccountIndex: 0,
    });
    const publicKeys = await getPublicKeys();
    expect(publicKeys.selected.addressIndex).toBe(0);
    expect(publicKeys.other).toHaveLength(2);
    expect(publicKeys.other[0]?.addressIndex).toBe(1);
    expect(publicKeys.other[1]?.addressIndex).toBe(2);
  });

  it('returns the correct selected account', async () => {
    snapRequest.mockResolvedValue({
      numberOfAccounts: 3,
      selectedAccountIndex: 2,
    });
    const publicKeys = await getPublicKeys();
    expect(publicKeys.selected.addressIndex).toBe(2);
    expect(publicKeys.other).toHaveLength(2);
    expect(publicKeys.other[0]?.addressIndex).toBe(0);
    expect(publicKeys.other[1]?.addressIndex).toBe(1);
  });

  it('fails if selected account is out of bounds', async () => {
    snapRequest.mockResolvedValue({
      numberOfAccounts: 3,
      selectedAccountIndex: 3,
    });
    const publicKeysPromise = getPublicKeys();
    await expect(publicKeysPromise).rejects.toMatchObject({
      code: -32603,
      message: 'Failed to find the selected account in your addresses',
      stack: expect.any(String),
    });
  });
});
