import { describe, expect } from '@jest/globals';
import { ManageStateOperation } from '@metamask/snaps-sdk';

import {
  chooseAccount,
  createAccount,
  getNumberOfAccounts,
  getSelectedAccountIndex,
  removeAccount,
} from '../src/multipleAccounts';
import { snapRequest } from './setupTests';

describe('multiple accounts', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('getNumberOfAccounts', () => {
    it('retrieves the number from state', async () => {
      snapRequest.mockResolvedValueOnce({
        numberOfAccounts: 3,
      });

      const numberOfAccounts = await getNumberOfAccounts();
      expect(numberOfAccounts).toBe(3);
    });

    it('returns one account on empty state', async () => {
      const numberOfAccounts = await getNumberOfAccounts();
      expect(numberOfAccounts).toBe(1);
    });
  });

  describe('getSelectedAccountIndex', () => {
    it('retrieves the number from state', async () => {
      snapRequest.mockResolvedValueOnce({
        selectedAccountIndex: 3,
      });

      const selectedAccountIndex = await getSelectedAccountIndex();
      expect(selectedAccountIndex).toBe(3);
    });

    it('returns index zero on empty state', async () => {
      const selectedAccountIndex = await getSelectedAccountIndex();
      expect(selectedAccountIndex).toBe(0);
    });
  });

  describe('chooseAccount', () => {
    it('updates the current account index', async () => {
      await chooseAccount(123);

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            selectedAccountIndex: 123,
            numberOfAccounts: 1,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });
  });

  describe('createAccount', () => {
    it('increments number of accounts', async () => {
      snapRequest.mockResolvedValueOnce({
        numberOfAccounts: 10,
        selectedAccountIndex: 0,
      });

      await createAccount(false);

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 11,
            selectedAccountIndex: 0,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('switches to the newly created account', async () => {
      snapRequest.mockResolvedValueOnce({
        numberOfAccounts: 10,
        selectedAccountIndex: 0,
      });

      await createAccount(true);

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 11,
            selectedAccountIndex: 10,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('sets number of accounts to 2 on empty state', async () => {
      await createAccount(false);
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 2,
            selectedAccountIndex: 0,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });
  });

  describe('removeAccount', () => {
    it('decrements number of accounts', async () => {
      snapRequest.mockResolvedValueOnce({
        numberOfAccounts: 10,
        selectedAccountIndex: 0,
      });

      await removeAccount();

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 9,
            selectedAccountIndex: 0,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('updates the selected account if it was removed', async () => {
      snapRequest.mockResolvedValueOnce({
        numberOfAccounts: 2,
        selectedAccountIndex: 1,
      });

      await removeAccount();

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 1,
            selectedAccountIndex: 0,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('fails if there is less than two accounts', async () => {
      const response = removeAccount();
      await expect(response).rejects.toMatchObject({
        code: -32603,
        message: 'Tried to remove an account, but only one account exist',
      });
    });
  });
});
