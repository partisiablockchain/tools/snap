import { beforeEach, describe, expect } from '@jest/globals';
import type {
  RequestOptions,
  Snap,
  SnapConfirmationInterface,
  SnapRequest,
} from '@metamask/snaps-jest';
import { installSnap } from '@metamask/snaps-jest';
import {
  Box,
  Copyable,
  Divider,
  Heading,
  Link,
  Row,
  Section,
  Text,
} from '@metamask/snaps-sdk/jsx';

describe('onRpcRequest', () => {
  it('throws an error if the requested method does not exist', async () => {
    const { request } = await installSnap();
    const response = await request({
      method: 'foo',
    });

    expect(response).toRespondWithError({
      code: -32601,
      message: 'The method does not exist / is not available.',
      stack: expect.any(String),
      data: {
        method: 'foo',
        cause: null,
      },
    });
  });

  describe('get_address', () => {
    it('returns an address', async () => {
      const { request } = await installSnap();
      const response = await request({
        method: 'get_address',
      });
      expect(response).toRespondWith(
        '003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab',
      );
    });
  });

  describe('sign_transaction', () => {
    it('signs a transaction on user accept', async () => {
      const { request } = await installSnap();
      const response = request({
        method: 'sign_transaction',
        params: {
          payload: `${'0'.repeat(15)}1${'0'.repeat(31)}202${'0'.repeat(66)}`,
          chainId: 'ChainId',
        },
      });
      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      expect(ui).toRender(
        <Box>
          <Heading>{'Confirm transaction signature on ChainId'}</Heading>
          <Text>
            {
              'You are currently using account: 003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab'
            }
          </Text>
          <Text>
            {
              'Sign transaction with nonce 1 and cost 2 towards contract 020000000000000000000000000000000000000000'
            }
          </Text>
        </Box>,
      );
      await ui.ok();
      expect(await response).toRespondWith(
        '00c7e3509e7db6cb591420d57d43d896f3d3b6f629755cd87e93d2876e029184abf92ad1631a29b26bae92a593aa69788f76d78db6aba677a006fdb7e1695a075e',
      );
    });

    it('fails when user rejects', async () => {
      const { request } = await installSnap();
      const response = request({
        method: 'sign_transaction',
        params: {
          payload: '0'.repeat(90),
          chainId: 'ChainId',
        },
      });
      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      await ui.cancel();
      expect(await response).toRespondWithError({
        code: 4001,
        message: 'User rejected the request.',
        stack: expect.any(String),
      });
    });

    it('reject: too short payload', async () => {
      const { request } = await installSnap();
      const response = await request({
        method: 'sign_transaction',
        params: {
          payload: '0'.repeat(88),
          chainId: 'ChainId',
        },
      });
      expect(response).toRespondWithError({
        code: -32602,
        message: 'Incomplete transaction',
        stack: expect.any(String),
      });
    });

    it('reject: malformed payload 1', async () => {
      const { request } = await installSnap();
      const response = await request({
        method: 'sign_transaction',
        params: {
          payload: '0'.repeat(90),
        },
      });
      expect(response).toRespondWithError({
        code: -32602,
        message: 'Malformed sign request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed payload 2', async () => {
      const { request } = await installSnap();
      const response = await request({
        method: 'sign_transaction',
        params: {
          chainId: 'ChainId',
        },
      });
      expect(response).toRespondWithError({
        code: -32602,
        message: 'Malformed sign request',
        stack: expect.any(String),
      });
    });
  });

  describe('get_accounts', () => {
    it('returns addresses with indexes', async () => {
      const { request } = await installSnap();
      const response = await request({
        method: 'get_accounts',
      });
      expect(response).toRespondWith({
        selected: {
          key: '003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab',
          addressIndex: 0,
        },
        other: [],
      });
    });
  });

  describe('create_account', () => {
    it('shows a confirmation dialog when not switching', async () => {
      const { request } = await installSnap();
      const response = request({
        method: 'create_account',
      });

      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      expect(ui).toRender(
        <Box>
          <Heading>{'Add a new account'}</Heading>
          <Text>{'Are you sure you want to add a new account?'}</Text>
          <Divider></Divider>
          <Text>{'The new account will not be selected when you add it'}</Text>
        </Box>,
      );
    });

    it('creates an account and returns the new addresses', async () => {
      const { request } = await installSnap();
      const response = request({
        method: 'create_account',
        params: {
          switchToNewAccount: true,
        },
      });

      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui).toRender(
        <Box>
          <Heading>{'Add a new account'}</Heading>
          <Text>{'Are you sure you want to add a new account?'}</Text>
          <Divider></Divider>
          <Text>
            {'The new account will automatically be selected when you add it'}
          </Text>
        </Box>,
      );
      await ui.ok();

      expect(await response).toRespondWith({
        selected: {
          key: expect.any(String),
          addressIndex: 1,
        },
        other: [
          {
            key: '003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab',
            addressIndex: 0,
          },
        ],
      });
    });

    it('fails when user rejects', async () => {
      const { request } = await installSnap();
      const response = request({
        method: 'create_account',
      });

      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      await ui.cancel();

      expect(await response).toRespondWithError({
        code: 4001,
        message: 'User rejected the request.',
        stack: expect.any(String),
      });
    });
  });

  describe('choose_account', () => {
    let request: (request: RequestOptions) => SnapRequest;

    beforeEach(async () => {
      // create an additional account
      ({ request } = await installSnap());
      const createAccountResponse = request({
        method: 'create_account',
      });

      const ui =
        (await createAccountResponse.getInterface()) as SnapConfirmationInterface;
      await ui.ok();
      await createAccountResponse;
    });

    it('shows a confirmation dialog', async () => {
      const response = request({
        method: 'choose_account',
        params: {
          addressIndex: 1,
        },
      });
      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      expect(ui).toRender(
        <Box>
          <Heading>{'Choose account'}</Heading>
          <Text>
            {'Are you sure you want to switch to the following PBC account?'}
          </Text>
          <Text>{'009a3bc1539a5ef82ac082be5a625f0f0c5249af6b'}</Text>
        </Box>,
      );
    });

    it('changes the selected account', async () => {
      const response = request({
        method: 'choose_account',
        params: {
          addressIndex: 1,
        },
      });

      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      await ui.ok();

      expect(await response).toRespondWith({
        selected: {
          key: '009a3bc1539a5ef82ac082be5a625f0f0c5249af6b',
          addressIndex: 1,
        },
        other: [
          {
            key: '003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab',
            addressIndex: 0,
          },
        ],
      });
    });

    it('fails when user rejects', async () => {
      const response = request({
        method: 'choose_account',
        params: {
          addressIndex: 1,
        },
      });
      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      await ui.cancel();

      expect(await response).toRespondWithError({
        code: 4001,
        message: 'User rejected the request.',
        stack: expect.any(String),
      });

      const accountsAfterReject = await request({
        method: 'get_accounts',
      });
      expect(accountsAfterReject).toRespondWith({
        selected: {
          key: '003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab',
          addressIndex: 0,
        },
        other: [
          {
            key: '009a3bc1539a5ef82ac082be5a625f0f0c5249af6b',
            addressIndex: 1,
          },
        ],
      });
    });
  });

  describe('remove_account', () => {
    let request: (request: RequestOptions) => SnapRequest;
    beforeEach(async () => {
      // create an additional account
      ({ request } = await installSnap());
      const createAccountResponse = request({
        method: 'create_account',
      });

      const ui =
        (await createAccountResponse.getInterface()) as SnapConfirmationInterface;
      await ui.ok();
      await createAccountResponse;
    });

    it('shows a confirmation and removes the account', async () => {
      const response = request({
        method: 'remove_account',
      });

      const ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      expect(ui).toRender(
        <Box>
          <Heading>{'Remove account'}</Heading>
          <Text>{'Are you sure you want to remove the following account'}</Text>
          <Row
            variant={'warning'}
            label={'009a3bc1539a5ef82ac082be5a625f0f0c5249af6b'}
          >
            <Text>{''}</Text>
          </Row>
          <Text>{''}</Text>
          <Divider />
          <Text>
            {
              'You can always get this account back by adding a new account again, this will derive the same account'
            }
          </Text>
        </Box>,
      );

      await ui.ok();
      expect(await response).toRespondWith({
        selected: {
          key: '003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab',
          addressIndex: 0,
        },
        other: [],
      });
    });

    it('shows additional warning if it is the active account', async () => {
      // First choose the account to be deleted
      let response = request({
        method: 'choose_account',
        params: {
          addressIndex: 1,
        },
      });
      let ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      await ui.ok();
      await response;

      response = request({
        method: 'remove_account',
      });

      ui = (await response.getInterface()) as SnapConfirmationInterface;
      expect(ui.type).toBe('confirmation');
      expect(ui).toRender(
        <Box>
          <Heading>{'Remove account'}</Heading>
          <Text>{'Are you sure you want to remove the following account'}</Text>
          <Row
            variant={'warning'}
            label={'009a3bc1539a5ef82ac082be5a625f0f0c5249af6b'}
          >
            <Text>{''}</Text>
          </Row>
          <Box>
            <Divider />
            <Row
              variant={'critical'}
              label={
                'This is your current active account, if removed the active account will be switched to the default account'
              }
            >
              <Text>{''}</Text>
            </Row>
          </Box>
          <Divider />
          <Text>
            {
              'You can always get this account back by adding a new account again, this will derive the same account'
            }
          </Text>
        </Box>,
      );

      await ui.ok();
      expect(await response).toRespondWith({
        selected: {
          key: '003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab',
          addressIndex: 0,
        },
        other: [],
      });
    });
  });

  describe('onHomePage', () => {
    let snap: Snap;
    beforeEach(async () => {
      snap = await installSnap();
    });

    it('shows homepage for single account', async () => {
      const homePageResponse = await snap.onHomePage();
      const ui = homePageResponse.getInterface();
      expect(ui).toRender(
        <Box>
          <Box direction={'horizontal'} alignment={'space-between'}>
            <Heading>{'See your assets'}</Heading>
            <Link href={'https://browser.partisiablockchain.com/assets'}>
              {'Assets'}
            </Link>
          </Box>
          <Box direction={'horizontal'} alignment={'space-between'}>
            <Heading>{'Bridge your coins'}</Heading>
            <Link href={'https://browser.partisiablockchain.com/bridge'}>
              {'Bridge'}
            </Link>
          </Box>
          <Box>
            <Heading>{'Your account'}</Heading>
            {[
              <Copyable
                value={'003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab'}
                key={'0-003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab'}
              />,
            ]}
          </Box>
        </Box>,
      );
    });

    it('shows homepage for multiple accounts', async () => {
      // create an additional account
      const createAccountResponse = snap.request({
        method: 'create_account',
      });

      const ui =
        (await createAccountResponse.getInterface()) as SnapConfirmationInterface;
      await ui.ok();
      await createAccountResponse;

      const homePageResponse = await snap.onHomePage();
      const homepageUi = homePageResponse.getInterface();
      expect(homepageUi).toRender(
        <Box>
          <Box direction={'horizontal'} alignment={'space-between'}>
            <Heading>{'See your assets'}</Heading>
            <Link href={'https://browser.partisiablockchain.com/assets'}>
              {'Assets'}
            </Link>
          </Box>
          <Box direction={'horizontal'} alignment={'space-between'}>
            <Heading>{'Bridge your coins'}</Heading>
            <Link href={'https://browser.partisiablockchain.com/bridge'}>
              {'Bridge'}
            </Link>
          </Box>
          <Box>
            <Heading>{'Accounts'}</Heading>
            {[
              <Section key={'0-003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab'}>
                <Text>{'Current account'}</Text>
                <Copyable
                  value={'003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab'}
                />
              </Section>,
              <Copyable
                value={'009a3bc1539a5ef82ac082be5a625f0f0c5249af6b'}
                key={'1-009a3bc1539a5ef82ac082be5a625f0f0c5249af6b'}
              />,
            ]}
          </Box>
        </Box>,
      );
    });

    it('shows homepage after account switch', async () => {
      // create an additional account
      const createAccountResponse = snap.request({
        method: 'create_account',
      });

      let ui =
        (await createAccountResponse.getInterface()) as SnapConfirmationInterface;
      await ui.ok();
      await createAccountResponse;

      // choose the other account
      const chooseAccountResponse = snap.request({
        method: 'choose_account',
        params: {
          addressIndex: 1,
        },
      });

      ui =
        (await chooseAccountResponse.getInterface()) as SnapConfirmationInterface;
      await ui.ok();
      await chooseAccountResponse;

      const homePageResponse = await snap.onHomePage();
      const homepageUi = homePageResponse.getInterface();
      expect(homepageUi).toRender(
        <Box>
          <Box direction={'horizontal'} alignment={'space-between'}>
            <Heading>{'See your assets'}</Heading>
            <Link href={'https://browser.partisiablockchain.com/assets'}>
              {'Assets'}
            </Link>
          </Box>
          <Box direction={'horizontal'} alignment={'space-between'}>
            <Heading>{'Bridge your coins'}</Heading>
            <Link href={'https://browser.partisiablockchain.com/bridge'}>
              {'Bridge'}
            </Link>
          </Box>
          <Box>
            <Heading>{'Accounts'}</Heading>
            {[
              <Copyable
                value={'003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab'}
                key={'0-003f86c6165373c3d02ff425f6cdcb1ed7bc3cd1ab'}
              />,
              <Section key={'1-009a3bc1539a5ef82ac082be5a625f0f0c5249af6b'}>
                <Text>{'Current account'}</Text>
                <Copyable
                  value={'009a3bc1539a5ef82ac082be5a625f0f0c5249af6b'}
                />
              </Section>,
            ]}
          </Box>
        </Box>,
      );
    });
  });
});
