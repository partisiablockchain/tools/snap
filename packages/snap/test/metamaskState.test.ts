import { expect } from '@jest/globals';
import { ManageStateOperation } from '@metamask/snaps-sdk';

import type { State } from '../src/metamaskState';
import {
  clearState,
  getState,
  setState,
  updateState,
} from '../src/metamaskState';
import { snapRequest } from './setupTests';

describe('metamask state', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('getState', () => {
    it('handles empty state', async () => {
      snapRequest.mockResolvedValueOnce(null);
      const state = await getState();
      expect(state).toStrictEqual({
        selectedAccountIndex: 0,
        numberOfAccounts: 1,
      });
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          operation: ManageStateOperation.GetState,
          encrypted: true,
        },
      });
    });

    it('gets the encrypted state', async () => {
      const state = {
        selectedAccountIndex: 123,
        numberOfAccounts: 456,
      };
      snapRequest.mockResolvedValueOnce(state);
      const result = await getState();
      expect(result).toBe(state);
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          operation: ManageStateOperation.GetState,
          encrypted: true,
        },
      });
    });

    it('gets the non-encrypted state', async () => {
      await getState(false);
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          operation: ManageStateOperation.GetState,
          encrypted: false,
        },
      });
    });
  });

  describe('setState', () => {
    it('sets the encrypted state', async () => {
      const state = {
        selectedAccountIndex: 123,
        numberOfAccounts: 456,
      };
      await setState(state);
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: state,
          operation: ManageStateOperation.UpdateState,
          encrypted: true,
        },
      });
    });

    it('sets the non-encrypted state', async () => {
      const state = {
        selectedAccountIndex: 123,
        numberOfAccounts: 456,
      };
      await setState(state, false);
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: state,
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });
  });

  describe('updateState', () => {
    it('updates a value and maintains others', async () => {
      const state = {
        selectedAccountIndex: 123,
        numberOfAccounts: 456,
      };
      snapRequest.mockResolvedValueOnce(state);

      await updateState({
        selectedAccountIndex: 0,
      });
      expect(snap.request).toHaveBeenNthCalledWith(1, {
        method: 'snap_manageState',
        params: {
          operation: ManageStateOperation.GetState,
          encrypted: true,
        },
      });
      expect(snap.request).toHaveBeenNthCalledWith(2, {
        method: 'snap_manageState',
        params: {
          newState: {
            selectedAccountIndex: 0,
            numberOfAccounts: 456,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: true,
        },
      });
    });

    it('updates the non-encrypted state', async () => {
      const newState = {
        numberOfAccounts: 456,
      };

      await updateState(newState, false);
      expect(snap.request).toHaveBeenNthCalledWith(1, {
        method: 'snap_manageState',
        params: {
          operation: ManageStateOperation.GetState,
          encrypted: false,
        },
      });
      expect(snap.request).toHaveBeenNthCalledWith(2, {
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 456,
            selectedAccountIndex: 0,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('uses the given update function', async () => {
      const myUpdateFunction = (prev: State) => {
        return {
          numberOfAccounts: prev.numberOfAccounts + 20,
        };
      };
      await updateState(myUpdateFunction);
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: { numberOfAccounts: 21, selectedAccountIndex: 0 },
          operation: ManageStateOperation.UpdateState,
          encrypted: true,
        },
      });
    });
  });

  describe('clearState', () => {
    it('clears the encrypted state', async () => {
      await clearState();
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          operation: ManageStateOperation.ClearState,
          encrypted: true,
        },
      });
    });

    it('clears the non-encrypted state', async () => {
      await clearState(false);
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          operation: ManageStateOperation.ClearState,
          encrypted: false,
        },
      });
    });
  });
});
