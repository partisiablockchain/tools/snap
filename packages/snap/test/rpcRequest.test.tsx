import { afterEach, beforeEach, describe, expect } from '@jest/globals';
import { getBIP44AddressKeyDeriver } from '@metamask/key-tree';
import type { Json } from '@metamask/snaps-sdk';
import { ManageStateOperation } from '@metamask/snaps-sdk';
import type { JsonRpcParams } from '@metamask/utils';
import { resetAllWhenMocks, when } from 'jest-when';

import { onRpcRequest } from '../src';
import { removeOnlyAccountAlert } from '../src/view/removeAccountConfirmation';
import { snapRequest } from './setupTests';

jest.mock('@metamask/key-tree');
const bip44KeyDeriverMock = getBIP44AddressKeyDeriver as jest.Mock;

describe('onRpcRequest', () => {
  let bip44DeriverIndex: number | undefined;
  beforeEach(() => {
    bip44KeyDeriverMock.mockResolvedValue((idx: number) => {
      bip44DeriverIndex = idx;
      return {
        privateKey: `0x11223344${idx.toString(16)}`,
      };
    });
  });
  afterEach(() => {
    bip44DeriverIndex = undefined;
    jest.resetAllMocks();
    resetAllWhenMocks();
  });

  it('throws an error if the requested method does not exist', async () => {
    const response = onRpcRequest({
      origin: 'TestOrigin',
      request: {
        id: null,
        method: 'foo',
        jsonrpc: '2.0',
        params: {},
      },
    });
    await expect(response).rejects.toMatchObject({
      code: -32601,
      message: 'The method does not exist / is not available.',
    });
  });

  describe('get_address', () => {
    it('returns an address', async () => {
      const response = await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'get_address',
          jsonrpc: '2.0',
          params: {},
        },
      });
      expect(response).toBe('00a01aae9e76e841c279a17e4b112df519a4c559af');
      expect(bip44DeriverIndex).toBe(0);
      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_getBip44Entropy',
        params: {
          coinType: 3757,
        },
      });
    });

    it('returns the selected accounts address', async () => {
      snapRequest.mockResolvedValueOnce({
        numberOfAccounts: 2,
        selectedAccountIndex: 1,
      });

      await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'get_address',
          jsonrpc: '2.0',
          params: {},
        },
      });
      expect(bip44DeriverIndex).toBe(1);
    });

    it('rejects when unable to get private key', async () => {
      bip44KeyDeriverMock.mockResolvedValueOnce(() => ({
        privateKey: undefined,
      }));
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'get_address',
          jsonrpc: '2.0',
          params: {},
        },
      });
      await expect(response).rejects.toMatchObject({
        code: -32603,
        message: 'Unable to derive BIP44 key',
      });
    });
  });

  describe('get_accounts', () => {
    it('returns addresses with indexes', async () => {
      snapRequest.mockResolvedValue({
        numberOfAccounts: 2,
        selectedAccountIndex: 1,
      });
      const response = await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'get_accounts',
          jsonrpc: '2.0',
          params: {},
        },
      });
      expect(response).toMatchObject({
        selected: {
          key: expect.any(String),
          addressIndex: 1,
        },
        other: [
          {
            key: expect.any(String),
            addressIndex: 0,
          },
        ],
      });
    });
  });

  describe('choose_account', () => {
    it('choose an account', async () => {
      // accept in dialog
      when(snapRequest)
        .calledWith({
          method: 'snap_dialog',
          params: expect.anything(),
        })
        .mockResolvedValueOnce(true);

      await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'choose_account',
          jsonrpc: '2.0',
          params: {
            addressIndex: 0,
          },
        },
      });

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: expect.objectContaining({
            selectedAccountIndex: 0,
          }),
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('fails on user reject', async () => {
      // reject in dialog
      when(snapRequest)
        .calledWith({
          method: 'snap_dialog',
          params: expect.anything(),
        })
        .mockResolvedValueOnce(false);

      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'choose_account',
          jsonrpc: '2.0',
          params: {
            addressIndex: 0,
          },
        },
      });

      await expect(response).rejects.toMatchObject({
        code: 4001,
        message: 'User rejected the request.',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params missing addressIndex', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'choose_account',
          jsonrpc: '2.0',
          params: {},
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed choose account request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params not an object', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'choose_account',
          jsonrpc: '2.0',
          params: [],
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed choose account request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params address index not a number', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'choose_account',
          jsonrpc: '2.0',
          params: {
            addressIndex: '0',
          },
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed choose account request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params undefined', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'choose_account',
          jsonrpc: '2.0',
          params: undefined as unknown as Record<string, Json>,
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed choose account request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params null', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'choose_account',
          jsonrpc: '2.0',
          params: null as unknown as Record<string, Json>,
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed choose account request',
        stack: expect.any(String),
      });
    });
  });

  describe('create_account', () => {
    it('creates a new account', async () => {
      // accept in dialog
      snapRequest.mockResolvedValueOnce(true);

      const result = await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'create_account',
          jsonrpc: '2.0',
          params: { switchToNewAccount: true },
        },
      });
      // ensure that it returns the keys, but since the state is mocked it will
      //  not return an additional key.
      expect(result).toMatchObject({
        selected: {
          key: expect.any(String),
          addressIndex: 0,
        },
        other: expect.anything(),
      });

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 2,
            selectedAccountIndex: 1,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('handles malformed parameters', async () => {
      const malformedParams: JsonRpcParams[] = [
        {},
        [],
        null as unknown as JsonRpcParams,
        undefined as unknown as JsonRpcParams,
        { switchToNewAccount: 'true' },
      ];

      for (const malformedParam of malformedParams) {
        // accept in dialog
        snapRequest.mockResolvedValueOnce(true);
        await onRpcRequest({
          origin: 'TestOrigin',
          request: {
            id: null,
            method: 'create_account',
            jsonrpc: '2.0',
            params: malformedParam,
          },
        });

        expect(snapRequest).toHaveBeenCalledWith({
          method: 'snap_manageState',
          params: {
            newState: {
              numberOfAccounts: 2,
              selectedAccountIndex: 0,
            },
            operation: ManageStateOperation.UpdateState,
            encrypted: false,
          },
        });
        snapRequest.mockClear();
      }
    });

    it('handles malformed parameters - null', async () => {
      // accept in dialog
      snapRequest.mockResolvedValueOnce(true);
      await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'create_account',
          jsonrpc: '2.0',
          params: null as unknown as Record<string, Json>,
        },
      });

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 2,
            selectedAccountIndex: 0,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('handles malformed parameters - undefined', async () => {
      // accept in dialog
      snapRequest.mockResolvedValueOnce(true);
      await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'create_account',
          jsonrpc: '2.0',
          params: undefined as unknown as Record<string, Json>,
        },
      });

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 2,
            selectedAccountIndex: 0,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('fails on user reject', async () => {
      // reject in dialog
      snapRequest.mockResolvedValueOnce(false);
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'create_account',
          jsonrpc: '2.0',
          params: {},
        },
      });

      await expect(response).rejects.toMatchObject({
        code: 4001,
        message: 'User rejected the request.',
        stack: expect.any(String),
      });
    });
  });

  describe('remove_account', () => {
    it('removes an account', async () => {
      // mock user accept and state lookup
      when(snapRequest)
        .calledWith({
          method: 'snap_dialog',
          params: expect.anything(),
        })
        .mockResolvedValueOnce(true)
        .defaultResolvedValue({
          numberOfAccounts: 2,
          selectedAccountIndex: 0,
        });

      const result = await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'remove_account',
          jsonrpc: '2.0',
          params: {},
        },
      });

      // ensure that it returns the keys, but since the state is mocked it will
      //  not exclude the removed account.
      expect(result).toMatchObject({
        selected: {
          key: expect.any(String),
          addressIndex: 0,
        },
        other: expect.arrayContaining([]),
      });

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_manageState',
        params: {
          newState: {
            numberOfAccounts: 1,
            selectedAccountIndex: 0,
          },
          operation: ManageStateOperation.UpdateState,
          encrypted: false,
        },
      });
    });

    it('fails on user reject', async () => {
      // mock user reject and state lookup
      when(snapRequest)
        .calledWith({
          method: 'snap_dialog',
          params: expect.anything(),
        })
        .mockResolvedValueOnce(false)
        .defaultResolvedValue({
          numberOfAccounts: 2,
          selectedAccountIndex: 1,
        });

      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'remove_account',
          jsonrpc: '2.0',
          params: {},
        },
      });

      await expect(response).rejects.toMatchObject({
        code: 4001,
        message: 'User rejected the request.',
        stack: expect.any(String),
      });
    });

    it('shows alert if less than two accounts exist', async () => {
      snapRequest.mockResolvedValueOnce({
        numberOfAccounts: 1,
        selectedAccountIndex: 0,
      });
      const result = await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'remove_account',
          jsonrpc: '2.0',
          params: {},
        },
      });

      expect(snap.request).toHaveBeenCalledWith({
        method: 'snap_dialog',
        params: removeOnlyAccountAlert(),
      });

      // ensure that it returns the keys, but since the state is mocked it will
      //  not exclude the removed account.
      expect(result).toMatchObject({
        selected: {
          key: expect.any(String),
          addressIndex: 0,
        },
        other: expect.arrayContaining([]),
      });
    });
  });

  describe('sign_transaction', () => {
    it('reject too short payload', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            payload: '0'.repeat(88),
            chainId: 'ChainId',
          },
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Incomplete transaction',
        stack: expect.any(String),
      });
    });

    it('fails when user rejects', async () => {
      // mock user reject
      when(snapRequest)
        .calledWith({
          method: 'snap_dialog',
          params: expect.anything(),
        })
        .mockResolvedValueOnce(false);

      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            payload: '0'.repeat(90),
            chainId: 'ChainId',
          },
        },
      });

      await expect(response).rejects.toMatchObject({
        code: 4001,
        message: 'User rejected the request.',
        stack: expect.any(String),
      });
    });

    it('signs a transaction on user accept', async () => {
      // mock user accept
      when(snapRequest)
        .calledWith({
          method: 'snap_dialog',
          params: expect.anything(),
        })
        .mockResolvedValueOnce(true);

      const response = await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            payload: `${'0'.repeat(15)}1${'0'.repeat(31)}202${'0'.repeat(66)}`,
            chainId: 'Partisia Blockchain',
          },
        },
      });
      expect(response).toBe(
        '009b72cf022925d688a9b9fa3073f3e2974a3004a3dbfe9d75310daa75c379fb5bd01387cd6f8647a2a029b45ffcd8eacd957b8c048fae438d15e871e9f500d7b5',
      );
      expect(bip44DeriverIndex).toBe(0);
    });

    it('signs using the correct key', async () => {
      // mock user accept and state lookup
      when(snapRequest)
        .calledWith({
          method: 'snap_dialog',
          params: expect.anything(),
        })
        .mockResolvedValueOnce(true)
        .defaultResolvedValue({
          numberOfAccounts: 2,
          selectedAccountIndex: 1,
        });
      await onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            payload: '0'.repeat(90),
            chainId: 'ChainId',
          },
        },
      });
      expect(bip44DeriverIndex).toBe(1);
    });

    it('reject: malformed params missing payload', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            chainId: 'ChainId',
          },
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed sign request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params missing chainId', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            payload: '0'.repeat(90),
          },
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed sign request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params not an object', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: [],
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed sign request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params undefined', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: undefined as unknown as Record<string, Json>,
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed sign request',
        stack: expect.any(String),
      });
    });

    it('reject: malformed params null', async () => {
      const response = onRpcRequest({
        origin: 'TestOrigin',
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: null as unknown as Record<string, Json>,
        },
      });

      await expect(response).rejects.toMatchObject({
        code: -32602,
        message: 'Malformed sign request',
        stack: expect.any(String),
      });
    });
  });
});
