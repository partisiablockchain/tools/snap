import type { SnapsProvider } from '@metamask/snaps-sdk';

// eslint-disable-next-line no-restricted-globals
const customGlobal: { snap: SnapsProvider } = global as unknown as {
  snap: SnapsProvider;
};
export const snapRequest = jest.fn();
customGlobal.snap = {
  request: snapRequest,
};
