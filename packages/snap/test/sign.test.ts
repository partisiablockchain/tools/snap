import { expect } from '@jest/globals';
import BN from 'bn.js';
import { Buffer } from 'buffer';
import type { ec as Elliptic } from 'elliptic';

import { signatureToBuffer, signTransaction } from '../src/sign';

describe('signatureToBuffer', () => {
  it('returns buffer from signature', () => {
    const buffer = signatureToBuffer({
      r: new BN(1),
      s: new BN(1),
      recoveryParam: 1,
    } as Elliptic.Signature);
    expect(buffer.toString('hex')).toBe(
      `01${'0'.repeat(63)}1${'0'.repeat(63)}1`,
    );
  });

  it('fails if recovery parameter is null', () => {
    expect(() =>
      signatureToBuffer({
        r: new BN(1),
        s: new BN(1),
        recoveryParam: null,
      } as Elliptic.Signature),
    ).toThrow('Recovery parameter is null');
  });
});

describe('signTransaction', () => {
  it('signTransaction', () => {
    const signature = signTransaction(
      Buffer.from('0000000000', 'hex'),
      'Partisia Blockchain',
      '112233',
    );
    expect(signature).toBe(
      '00c60a53ba9b2e734efeb36bafc3977eff78f2a5530d177e587f05de57e63f2776c6c0fdfa91690ee76f7986db270c185159725667b29e0adec019a631aa515f9a',
    );
  });
});
