import { useFormat } from '../src/format';

describe('format address', () => {
  it('shortens the address', () => {
    const format = useFormat();
    expect(format.address('0015a0e4989adb2104ee025f8fa26875eab613e377')).toBe(
      '0015a...377',
    );
  });
});
