import { afterEach, beforeEach, describe, expect } from '@jest/globals';
import { getBIP44AddressKeyDeriver } from '@metamask/key-tree';
import type { JSXElement } from '@metamask/snaps-sdk/jsx';
import {
  Box,
  Copyable,
  Heading,
  Link,
  Section,
  Text,
} from '@metamask/snaps-sdk/jsx';
import { resetAllWhenMocks } from 'jest-when';

import { onHomePage } from '../src';
import { snapRequest } from './setupTests';

jest.mock('@metamask/key-tree');
const bip44KeyDeriverMock = getBIP44AddressKeyDeriver as jest.Mock;

describe('onHomePage', () => {
  let bip44DeriverIndex: number | undefined;
  beforeEach(() => {
    bip44KeyDeriverMock.mockResolvedValue((idx: number) => {
      bip44DeriverIndex = idx;
      return {
        privateKey: `0x11223344${idx.toString(16)}`,
      };
    });
  });
  afterEach(() => {
    bip44DeriverIndex = undefined;
    jest.resetAllMocks();
    resetAllWhenMocks();
  });

  it('shows a homepage for single account', async () => {
    const response = (await onHomePage()) as {
      content: JSXElement;
    };
    expect(bip44DeriverIndex).toBe(0);
    expect(snap.request).toHaveBeenCalledWith({
      method: 'snap_getBip44Entropy',
      params: {
        coinType: 3757,
      },
    });
    expect(response.content).toStrictEqual(
      <Box>
        <Box direction={'horizontal'} alignment={'space-between'}>
          <Heading>{'See your assets'}</Heading>
          <Link href={'https://browser.partisiablockchain.com/assets'}>
            {'Assets'}
          </Link>
        </Box>
        <Box direction={'horizontal'} alignment={'space-between'}>
          <Heading>{'Bridge your coins'}</Heading>
          <Link href={'https://browser.partisiablockchain.com/bridge'}>
            {'Bridge'}
          </Link>
        </Box>
        <Box>
          <Heading>{'Your account'}</Heading>
          {[
            <Copyable
              value={'00a01aae9e76e841c279a17e4b112df519a4c559af'}
              key={'0-00a01aae9e76e841c279a17e4b112df519a4c559af'}
            />,
          ]}
        </Box>
      </Box>,
    );
  });

  it('shows a homepage for multiple accounts', async () => {
    snapRequest.mockResolvedValue({
      numberOfAccounts: 2,
      selectedAccountIndex: 1,
    });
    const response = (await onHomePage()) as {
      content: JSXElement;
    };
    expect(response.content).toStrictEqual(
      <Box>
        <Box direction={'horizontal'} alignment={'space-between'}>
          <Heading>{'See your assets'}</Heading>
          <Link href={'https://browser.partisiablockchain.com/assets'}>
            {'Assets'}
          </Link>
        </Box>
        <Box direction={'horizontal'} alignment={'space-between'}>
          <Heading>{'Bridge your coins'}</Heading>
          <Link href={'https://browser.partisiablockchain.com/bridge'}>
            {'Bridge'}
          </Link>
        </Box>
        <Box>
          <Heading>{'Accounts'}</Heading>
          {[
            <Copyable
              value={'00a01aae9e76e841c279a17e4b112df519a4c559af'}
              key={'0-00a01aae9e76e841c279a17e4b112df519a4c559af'}
            />,
            <Section key={'1-001c0c533d9ed33481bdda62287442c0546744489e'}>
              <Text>{'Current account'}</Text>
              <Copyable value={'001c0c533d9ed33481bdda62287442c0546744489e'} />
            </Section>,
          ]}
        </Box>
      </Box>,
    );
  });
});
