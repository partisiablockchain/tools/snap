# Partisia Blockchain Snap

This repository contains the Partisia Blockchain snap and a development site for testing the snap.

## How do I use the snap

Are you a dapp developer and end-user you should visit
the [Partisia Blockchain documentation](https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/metamask-snap-integration.html).

## Prerequisites

You must have the [MetaMask flask](https://docs.metamask.io/snaps/get-started/install-flask/)
installed.

Install yarn globally:

```shell
npm i --global yarn
```

## Developing

Building and running the snap is done using yarn commands.

### Install

```shell
yarn install
```

### Build and run

```shell
yarn start
```

### Using

To use the snap:

1. Open the [development site](http://localhost:8000).
2. Press the reconnect button to make sure the snap is connected.
3. Provide a transaction payload to sign:

   ```hex
   00000000000001fd0000018f5843cf9300000000000186a002c14c29b2697f3c983ada0ee7fac83f8a937e2ecd0000001a89fae4ff01008d4296154ed5cdb25130d7e188ca6b7e74032a17
   ```

4. Click the send message button

## Testing and Linting

Run `yarn test` to run the tests once.

Run `yarn lint` to run the linter, or run `yarn lint:fix` to run the linter and fix any
automatically fixable issues.
